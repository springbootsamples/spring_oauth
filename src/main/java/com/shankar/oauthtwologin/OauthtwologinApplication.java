package com.shankar.oauthtwologin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthtwologinApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthtwologinApplication.class, args);
	}
}

package com.shankar.oauthtwologin.service;

import java.util.List;

import com.shankar.oauthtwologin.model.User;

public interface UserService {
	
	User save(User user);
    List<User> findAll();
    void delete(long id);
}
